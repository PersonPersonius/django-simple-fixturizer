1.Place "fixture4.py" into the project folder(The one containing manage.py).
2.Open an idle in the project's directory using: python manage.py shell
3.import fixture4
4.fixture4.generate("home", "Person", 50)
5."home" is an app within the project, "Person" is a model within the app, 50 is the number of rows to be created.

This is a toy program.